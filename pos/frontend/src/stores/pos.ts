import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/product'

export const usePosStore = defineStore('pos', () => {
  const loadingStore = useLoadingStore()

  const drinkProducts = ref<Product[]>([])
  const bakeryProducts = ref<Product[]>([])
  const foodProducts = ref<Product[]>([])

  async function getProducts() {
    try {
      loadingStore.doLoad()
      let res = await productService.getProductsByType(1)
      drinkProducts.value = res.data

      res = await productService.getProductsByType(2)
      bakeryProducts.value = res.data

      res = await productService.getProductsByType(3)
      foodProducts.value = res.data

      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  return { drinkProducts, bakeryProducts, foodProducts, getProducts }
})
