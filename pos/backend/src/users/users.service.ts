import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/roles/entities/role.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  create(createUserDto: CreateUserDto) {
    const user = new User();
    user.email = createUserDto.email;
    user.fullName = createUserDto.fullName;
    user.gender = createUserDto.gender;
    user.password = createUserDto.password;
    user.roles = JSON.parse(createUserDto.roles);
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }
    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({ relations: { roles: true } });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneOrFail({
      where: { email },
    });
  }
  // ฟังก์ชั่นเริ่มแรกไม่ work
  // async update(id: number, updateUserDto: UpdateUserDto) {
  //   const updateUser = await this.usersRepository.findOneOrFail({
  //     where: { id },
  //   });
  //   await this.usersRepository.update(id, {
  //     ...updateUser,
  //     ...updateUserDto,
  //   });
  //   const result = await this.usersRepository.findOne({
  //     where: { id },
  //     relations: { roles: true },
  //   });
  //   return result;
  // }

  //ฟังก์ชั่นต่อมาจาก chatgpt work ใช้งานได้ แต่ entity ต้องเป็นก่อนที่อาจารย์จะแก้ พวก CreateDTO
  // async update(id: number, updateUserDto: UpdateUserDto) {
  //   // หาผู้ใช้ด้วย ID และตรวจสอบว่ามีอยู่จริง
  //   const updateUser = await this.usersRepository.findOneOrFail({
  //     where: { id },
  //   });

  //   // อัปเดตผู้ใช้ด้วยข้อมูลใหม่
  //   await this.usersRepository.save({
  //     ...updateUser,
  //     ...updateUserDto,
  //   });

  //   // โหลดข้อมูลผู้ใช้อีกครั้งพร้อมกับ roles ที่เกี่ยวข้อง
  //   const result = await this.usersRepository.findOne({
  //     where: { id },
  //     relations: ['roles'], // ใช้ array ของ string สำหรับชื่อ relation
  //   });

  //   return result;
  // }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.fullName = updateUserDto.fullName;
    user.gender = updateUserDto.gender;
    user.password = updateUserDto.password;
    user.roles = JSON.parse(updateUserDto.roles);
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }

    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: { roles: true },
    });

    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.image = user.image;
    //เพิ่ม role
    for (const r of user.roles) {
      const searchRole = updateUser.roles.find((role) => role.id === r.id);
      if (!searchRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }
    //ลด role
    //updateUserDTO ข้อมูลใหม่
    //updateUser ข้อมูลเดิม
    for (const r of updateUser.roles) {
      const unseeninDTO = user.roles.find((role) => role.id === r.id);
      if (!unseeninDTO) {
        const delIndex = updateUser.roles.findIndex((role) => role.id === r.id);
        updateUser.roles.splice(delIndex, 1);
      }
    }
    await this.usersRepository.save(updateUser);
    // โหลดข้อมูลผู้ใช้อีกครั้งพร้อมกับ roles ที่เกี่ยวข้อง
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: ['roles'], // ใช้ array ของ string สำหรับชื่อ relation
    });

    return result;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteUser);

    return deleteUser;
  }
}
